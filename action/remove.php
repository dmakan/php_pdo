<?php
    if (isset($_GET["table"])) {
        $table = $_GET['table'];
        $id = $_GET['id'];
    } 
?>


<?php
    
    include "../pdo/dbconn.php";
    include "../layout/base.php";

    $sql_request = "DELETE FROM {$table} WHERE id={$id};";

    $request = $CONN -> prepare($sql_request);
    $request -> execute();

    // header ("Location : ../?table=".$table);
?>


<body class="body-alert">
    <div class="container">
        <div class="row">
            
            
            <div class="col-md-4"> 
            </div>

            <div class="col-md-4" id="alert-suppr"> 
                <h5 class="alert alert-success">Votre action a été prise en compte.</h5>
                <p>La suppression a été effectuer avec succès</p>

                <a href="../?table=<?=$table?>"><button class='btn btn-primary'>Retour</button>
            </div>

            <div class="col-md-4"> 
            </div>
            
        </div>
    </div>
</body>